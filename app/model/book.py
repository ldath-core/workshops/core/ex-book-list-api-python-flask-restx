from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    author = db.Column(db.String, nullable=False)
    year = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String, nullable=True)
    img_type = db.Column(db.String, nullable=True)
    img_data = db.Column(db.String, nullable=True)

    def __init__(self, title: str, author: str, year: str, description='', img_type='', img_data=''):
        self.title = title
        self.author = author
        self.year = year
        self.description = description
        self.img_type = img_type
        self.img_data = img_data
