from flask_restx.reqparse import RequestParser
from sqlalchemy import func

from app.converter.converter import convert_book_entity_to_dto
from app.dto.v1.books_output import BooksOutput, PaginatedBooksContent
from app.dto.v1.create_book_output import CreateBookOutput, CreatedBook
from app.model.book import db, Book
from app.validator.validator import is_book_input_valid, InvalidRequestBodyError


class BookListHandler:

    @staticmethod
    def get(pagination_parser: RequestParser) -> BooksOutput:
        args = pagination_parser.parse_args()
        skip = args.get('skip')
        limit = args.get('limit')

        count = db.session.query(Book).count()

        results_partitions = db.session.execute(db.select(Book).order_by(Book.id)).scalars().partitions(limit)

        already_skipped = 0
        results = ()
        for partition in results_partitions:
            if already_skipped < skip:
                already_skipped += 1
            else:
                results = partition
                break

        return BooksOutput(
            200,
            'books - skip: %d; limit: %d' % (skip, limit),
            PaginatedBooksContent(
                count,
                skip,
                limit,
                tuple(convert_book_entity_to_dto(result) for result in results)))

    @staticmethod
    def post(request_body: dict) -> CreateBookOutput:
        try:
            is_book_input_valid(request_body)
        except InvalidRequestBodyError as e:
            return CreateBookOutput(400, str(e), None)

        book = Book(request_body['title'], request_body['author'], request_body['year'])

        if 'description' in request_body:
            book.description = request_body['description']

        if 'image' in request_body:
            if 'type' in request_body['image']:
                book.img_type = request_body['image']['type']
            if 'data' in request_body['image']:
                book.img_data = request_body['image']['data']

        db.session.add(book)
        db.session.commit()

        return CreateBookOutput(
            201,
            'book: %d created' % book.id,
            CreatedBook(book.id)
        )
