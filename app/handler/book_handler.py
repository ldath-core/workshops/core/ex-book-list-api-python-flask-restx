from typing import Union

from app.dto.v1.book_object import BookObject, Image
from app.dto.v1.book_output import BookOutput
from app.dto.v1.error import Error
from app.dto.v1.success import Success
from app.model.book import db, Book
from app.validator.validator import is_book_input_valid, InvalidRequestBodyError, is_book_input_update_valid


class BookHandler:

    @staticmethod
    def get(id: int) -> Union[BookOutput, Error]:
        book = db.session.query(Book).get(id)

        if book is None:
            return Error(404, 'there is no documents with this id', [])

        return BookOutput(
            200,
            'book',
            BookObject(
                book.id,
                book.title,
                book.author,
                book.year,
                book.description,
                Image(book.img_type, book.img_data)))

    @staticmethod
    def put(id: int, request_body: dict) -> Union[BookOutput, Error]:
        try:
            is_book_input_update_valid(request_body)
        except InvalidRequestBodyError as e:
            return Error(400, str(e), None)

        book = db.session.query(Book).get(id)
        if book is None:
            return Error(404, 'there is no documents with this id', [])

        if 'title' in request_body:
            book.title = request_body['title']
        if 'author' in request_body:
            book.author = request_body['author']
        if 'year' in request_body:
            book.year = request_body['year']
        if 'description' in request_body:
            book.description = request_body['description']

        if 'image' in request_body:
            if 'type' in request_body['image']:
                book.img_type = request_body['image']['type']
            if 'data' in request_body['image']:
                book.img_data = request_body['image']['data']

        db.session.commit()

        return BookOutput(
            202,
            'book: %d updated' % id,
            BookObject(
                book.id,
                book.title,
                book.author,
                book.year,
                book.description,
                Image(book.img_type, book.img_data)))

    @staticmethod
    def delete(id: int) -> Union[Success, Error]:
        book = db.session.query(Book).get(id)

        if book is None:
            return Error(404, 'there is no documents with this id', [])

        db.session.delete(book)
        db.session.commit()
        return Success(202, 'book: %d deleted' % id, '')
