from urllib.parse import urlparse

from psycopg2 import connect, OperationalError
from flask import current_app

from app.dto.v1.health import Health, HealthContent


class HealthHandler:

    @staticmethod
    def get():
        postgres = True
        url = urlparse(current_app.config['SQLALCHEMY_DATABASE_URI'])

        try:
            connection = connect(
                database=url.path[1:],
                user=url.username,
                password=url.password,
                host=url.hostname,
                port=url.port,
                connect_timeout=1)
            connection.close()
        except OperationalError:
            postgres = False

        return Health(200, 'ex-book-list-api-go api health', HealthContent(alive=True, postgres=postgres))
