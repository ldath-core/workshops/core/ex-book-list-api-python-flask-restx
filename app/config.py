from os import environ
from pathlib import Path

import yaml
import os

def parse_config(path: str):
    with open(path) as f:
        try:
            return yaml.safe_load(f)
        except yaml.YAMLError as e:
            raise ('Reading config error', e)


config = parse_config(environ.get('CFG_FILE', default=Path(__file__).parent.parent.joinpath('secrets/local.env.yaml')))

APP_NAME = 'ex_book_list_api'
RESTX_MASK_SWAGGER = False
SQLALCHEMY_DATABASE_URI = config['postgres']['url'].replace('postgres://', 'postgresql://')

SQLALCHEMY_TRACK_MODIFICATIONS = False
