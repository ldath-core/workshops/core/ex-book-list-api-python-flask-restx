from app.dto.v1.book_object import ImageType


class InvalidRequestBodyError(Exception):
    pass


def is_book_input_valid(request_body: dict):
    has_request_body_required_non_empty_string_field(request_body, 'title')
    has_request_body_required_non_empty_string_field(request_body, 'author')
    has_request_body_required_non_empty_string_field(request_body, 'year')

    has_request_body_no_unexpected_fields(request_body, ('title', 'author', 'year', 'description', 'image'))

    if 'description' in request_body:
        has_request_body_non_empty_string_field(request_body, 'description')

    if 'image' in request_body:
        type_defined = 'type' in request_body['image']
        data_defined = 'data' in request_body['image']

        if not type_defined and data_defined:
            raise InvalidRequestBodyError('image type is required if image data is set')
        if type_defined and not data_defined:
            raise InvalidRequestBodyError('image data is required if image type is set')

        if type_defined and data_defined:
            has_request_body_non_empty_string_field(request_body['image'], 'type')
            has_request_body_non_empty_string_field(request_body['image'], 'data')

        has_request_body_no_unexpected_fields(request_body['image'], ('type', 'data'))

        valid_types = tuple(type.value for type in ImageType)
        if request_body['image']['type'] not in valid_types:
            raise InvalidRequestBodyError('image type must be one of: ' + ', '.join(valid_types))


def is_book_input_update_valid(request_body: dict):
    if len(request_body) == 0:
        raise InvalidRequestBodyError('request body must not be empty')

    if 'title' in request_body:
        has_request_body_non_empty_string_field(request_body, 'title')
    if 'author' in request_body:
        has_request_body_non_empty_string_field(request_body, 'author')
    if 'year' in request_body:
        has_request_body_non_empty_string_field(request_body, 'year')

    has_request_body_no_unexpected_fields(request_body, ('title', 'author', 'year', 'description', 'image'))

    if 'description' in request_body:
        has_request_body_non_empty_string_field(request_body, 'description')

    if 'image' in request_body:
        type_defined = 'type' in request_body['image']
        data_defined = 'data' in request_body['image']

        if type_defined:
            has_request_body_non_empty_string_field(request_body['image'], 'type')

        if data_defined:
            has_request_body_non_empty_string_field(request_body['image'], 'data')

        has_request_body_no_unexpected_fields(request_body['image'], ('type', 'data'))

        valid_types = tuple(type.value for type in ImageType)
        if request_body['image']['type'] not in valid_types:
            raise InvalidRequestBodyError('image type must be one of: ' + ', '.join(valid_types))


def has_request_body_required_non_empty_string_field(request_body: dict, field_name: str):
    if field_name not in request_body:
        raise InvalidRequestBodyError(field_name + ' key is required')
    has_request_body_non_empty_string_field(request_body, field_name)


def has_request_body_non_empty_string_field(request_body: dict, field_name: str):
    if not isinstance(request_body[field_name], str):
        raise InvalidRequestBodyError(field_name + ' must be string')
    if request_body[field_name] == '':
        raise InvalidRequestBodyError(field_name + ' must not be empty')
    if request_body[field_name].isspace():
        raise InvalidRequestBodyError(field_name + ' must not be blank')


def has_request_body_no_unexpected_fields(request_body: dict, allowed_fields: tuple):
    for field in request_body.keys():
        if field not in allowed_fields:
            raise InvalidRequestBodyError('only keys: %s are allowed' % ', '.join(allowed_fields))
