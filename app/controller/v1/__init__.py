from flask import Blueprint
from flask_restx import Api

from app.controller.v1.book_controller import BookApi
from app.controller.v1.health_controller import HealthApi

blueprint_v1 = Blueprint('v1', __name__, url_prefix='/v1')

api_v1 = Api(blueprint_v1)
api_v1.title = 'ex-book-list-api-python-flask-restx'
api_v1.version = '1.0.0'
api_v1.description = 'This is a simple API'

api_v1.add_namespace(BookApi.api)
api_v1.add_namespace(HealthApi.api)
