from flask_restx import Resource, reqparse, marshal

from app.controller.v1.book_api import BookApi
from app.controller.v1.pagination_parser import pagination_parser
from app.handler.book_handler import BookHandler
from app.handler.book_list_handler import BookListHandler


@BookApi.api.route('/')
class BookListController(Resource):

    @BookApi.api.doc(
        'Get Books',
        description='This endpoint is sending details of all book-lists')
    @BookApi.api.expect(pagination_parser)
    @BookApi.api.response(200, 'Success response for the book-lists request', BookApi.books_output)
    @BookApi.api.response('default', 'Unexpected error', BookApi.error)
    @BookApi.api.marshal_list_with(BookApi.books_output)
    def get(self):
        result = BookListHandler.get(pagination_parser)
        return result, result.status

    @BookApi.api.doc(
        'Create Book',
        description='This endpoint creating new book-list and is sending details of book-list with assigned ID')
    @BookApi.api.expect(BookApi.create_book_input)
    @BookApi.api.response(201, 'Success response for the book-list creation request', BookApi.created_book_output)
    @BookApi.api.response('default', 'Unexpected error', BookApi.error)
    @BookApi.api.marshal_with(BookApi.created_book_output, code=201)
    def post(self):
        result = BookListHandler.post(reqparse.request.json)
        return result, result.status


@BookApi.api.route('/<int:id>')
class BookController(Resource):

    @BookApi.api.doc(
        'Get Book by ID',
        description='This endpoint is sending details of book-list chosen by ID')
    @BookApi.api.response(200, 'Success response for the book-list request', BookApi.book_output)
    @BookApi.api.response(404, 'Not Found', BookApi.error)
    @BookApi.api.response('default', 'Unexpected error', BookApi.error)
    def get(self, id: int):
        result = BookHandler.get(id)
        if result.status == 200:
            return marshal(result, BookApi.book_output), 200
        return marshal(result, BookApi.error), result.status

    @BookApi.api.doc(
        'Put Book by ID',
        description='This endpoint is updating book-list')
    @BookApi.api.expect(BookApi.update_book_input)
    @BookApi.api.response(202, 'Success response for the book-list request', BookApi.book_output)
    @BookApi.api.response(404, 'Not Found', BookApi.error)
    @BookApi.api.response('default', 'Unexpected error', BookApi.error)
    def put(self, id: int):
        result = BookHandler.put(id, reqparse.request.json)
        if result.status == 202:
            return marshal(result, BookApi.book_output), 202
        return marshal(result, BookApi.error), result.status

    @BookApi.api.doc(
        'Delete Book by ID',
        description='This endpoint is deleting book-list')
    @BookApi.api.response(202, 'Default Success response', BookApi.success)
    @BookApi.api.response(404, 'Not Found', BookApi.error)
    @BookApi.api.response('default', 'Unexpected error', BookApi.error)
    def delete(self, id: int):
        result = BookHandler.delete(id)
        if result.status == 202:
            return marshal(result, BookApi.success), 202
        return marshal(result, BookApi.error), result.status
