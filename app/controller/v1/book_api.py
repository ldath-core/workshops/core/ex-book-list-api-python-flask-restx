from flask_restx import Namespace

from app.dto.v1.book_object import BookObject, Image
from app.dto.v1.book_output import BookOutput
from app.dto.v1.books_output import PaginatedBooksContent, BooksOutput
from app.dto.v1.create_book_input import CreateBookInput
from app.dto.v1.create_book_output import CreatedBook, CreateBookOutput
from app.dto.v1.error import Error
from app.dto.v1.success import Success
from app.dto.v1.update_book_input import UpdateBookInput


class BookApi:

    api = Namespace('book-list', path='/books', description='Book related endpoints')

    # initialize response models
    success = api.model('Success', Success.get_model())
    error = api.model('Error', Error.get_model())

    image = api.model('Image', Image.get_model())

    book_object = api.model('BookObject', BookObject.get_model(image))

    paginated_books_content = api.model('PaginatedBooksContent', PaginatedBooksContent.get_model(book_object))
    books_output = api.model('BooksOutput', BooksOutput.get_model(paginated_books_content))

    created_book = api.model('CreatedBook', CreatedBook.get_model())
    created_book_output = api.model('CreateBookOutput', CreateBookOutput.get_model(created_book))

    book_output = api.model('BookOutput', BookOutput.get_model(book_object))

    # initialize request models
    create_book_input = api.model('CreateBookInput', CreateBookInput.get_model(image))

    update_book_input = api.model('UpdateBookInput', UpdateBookInput.get_model(image))
