from flask_restx import Namespace

from app.dto.v1.error import Error
from app.dto.v1.health import HealthContent, Health


class HealthApi:

    api = Namespace('health', path='/health', description='Get Health')

    error = api.model('Error', Error.get_model())

    health_content = api.model('HealthContent', HealthContent.get_model())
    health = api.model('Health', Health.get_model(health_content))
