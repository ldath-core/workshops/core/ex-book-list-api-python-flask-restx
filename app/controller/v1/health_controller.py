from flask_restx import Resource, marshal

from app.controller.v1.health_api import HealthApi
from app.handler.health_handler import HealthHandler


@HealthApi.api.route('/')
class HealthController2(Resource):

    @HealthApi.api.doc(
        'Get Health',
        description='This endpoint is sending health information')
    @HealthApi.api.response(200, 'Health information', HealthApi.health)
    @HealthApi.api.response(500, 'Internal Server Error', HealthApi.error)
    @HealthApi.api.response(501, 'Not Implemented', HealthApi.error)
    def get(self):
        result = HealthHandler.get()
        if result.status == 200:
            return marshal(result, HealthApi.health), 200
        return marshal(result, HealthApi.error), result.status
