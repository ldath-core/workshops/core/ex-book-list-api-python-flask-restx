from app.dto.v1.book_object import BookObject, Image
from app.model.book import Book


def convert_book_entity_to_dto(entity: Book) -> BookObject:
    return BookObject(
        entity.id,
        entity.title,
        entity.author,
        entity.year,
        entity.description,
        Image(entity.img_type, entity.img_data))
