from typing import Dict

from flask_restx.fields import Integer, String, Raw


class Success:
    def __init__(self, status: int, message: str, content: str):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model() -> Dict:
        return {
            'status': Integer(default=200),
            'message': String(),
            'content': String(nullable=True)}
