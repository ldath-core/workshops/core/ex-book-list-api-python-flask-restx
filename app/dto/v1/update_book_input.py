from typing import Dict

from flask_restx import Model
from flask_restx.fields import String, Nested


class UpdateBookInput:
    @staticmethod
    def get_model(image: Model) -> Dict:
        return {
            'title': String(required=False),
            'author': String(required=False),
            'year': String(required=False),
            'description': String(required=False),
            'image': Nested(image, required=False)}
