from enum import Enum
from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested


class ImageType(Enum):
    BASE64 = 'base64'
    URL = 'url'


class Image:
    def __init__(self, type: ImageType, data: str):
        self.type = type
        self.data = data

    @staticmethod
    def get_model() -> Dict:
        return {
            'type': String(required=True, enum=tuple(type.value for type in ImageType)),
            'data': String(required=True)
        }


class BookObject:
    def __init__(self, id: int, title: str, author: str, year: str, description='', image: Image = None):
        self.id = id
        self.title = title
        self.author = author
        self.year = year
        self.description = description
        self.image = image

    @staticmethod
    def get_model(image: Model) -> Dict:
        return {
            'id': Integer(required=True, readOnly=True, description='ID of the Book in the Database'),
            'title': String(required=True, nullable=False),
            'author': String(required=True, nullable=False),
            'year': String(required=True, nullable=False),
            'description': String(required=False),
            'image': Nested(image, required=False)}
