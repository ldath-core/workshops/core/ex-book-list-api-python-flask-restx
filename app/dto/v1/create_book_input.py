from typing import Dict

from flask_restx import Model
from flask_restx.fields import String, Nested


class CreateBookInput:
    @staticmethod
    def get_model(image: Model) -> Dict:
        return {
            'title': String(required=True, nullable=False),
            'author': String(required=True, nullable=False),
            'year': String(required=True, nullable=False),
            'description': String(required=False),
            'image': Nested(image, required=False)}
