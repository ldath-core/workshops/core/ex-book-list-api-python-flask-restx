from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested


class CreatedBook:
    def __init__(self, id: int):
        self.id = id

    @staticmethod
    def get_model() -> Dict:
        return {
            'id': Integer(description='id of the created object')
        }


class CreateBookOutput:
    def __init__(self, status: int, message: str, content: CreatedBook):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(created_book: Model) -> Dict:
        return {
            'status': Integer(default=201),
            'message': String(),
            'content': Nested(created_book)}
