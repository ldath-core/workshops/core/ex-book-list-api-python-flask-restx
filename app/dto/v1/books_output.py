from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested, List


class PaginatedBooksContent:
    def __init__(self, count: int, skip: int, limit: int, results: tuple):
        self.count = count
        self.skip = skip
        self.limit = limit
        self.results = results

    @staticmethod
    def get_model(book_object: Model) -> Dict:
        return {
            'count': Integer(required=True, example=42),
            'skip': Integer(required=True, example=0, default=0),
            'limit': Integer(required=True, example=10, default=10),
            'results': List(Nested(book_object), required=True)}


class BooksOutput:
    def __init__(self, status: int, message: str, content: PaginatedBooksContent):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(paginated_books_content: Model) -> Dict:
        return {
            'status': Integer(required=True, default=200, example=200),
            'message': String(required=True),
            'content': Nested(paginated_books_content, required=True)}
