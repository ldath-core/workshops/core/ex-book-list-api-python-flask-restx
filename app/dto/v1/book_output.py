from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested

from app.dto.v1.book_object import BookObject


class BookOutput:
    def __init__(self, status: int, message: str, content: BookObject):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(book_object: Model) -> Dict:
        return {
            'status': Integer(required=True, default=200, example=200),
            'message': String(required=True),
            'content': Nested(book_object, required=True)}
