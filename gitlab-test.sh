#!/usr/bin/env bash
set -e
REPO_DIR="$(pwd)"
cd tests
CFG_FILE="${REPO_DIR}/secrets/ci.env.yaml" PYTHONPATH="${REPO_DIR}" pytest
