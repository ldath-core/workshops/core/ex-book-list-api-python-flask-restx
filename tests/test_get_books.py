from pytest import mark

from test_fixtures import *
from tests.test_books import TestBooks


@mark.usefixtures('client')
class TestGetBooks:

    def test_should_get_all_books(self, client):
        # when
        response = client.get('/v1/books/')

        # then
        TestBooks.check_response(response, 200, 'books - skip: 0; limit: 10')
        TestGetBooks.check_pagination(response, 3, 0, 10, 3)

    def test_should_get_limited_books_number(self, client):
        # given
        skip = 0
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestBooks.check_response(response, 200, 'books - skip: %d; limit: %d' % (skip, limit))
        TestGetBooks.check_pagination(response, 3, skip, limit, 2)

    def test_should_skip_first_page(self, client):
        # given
        skip = 1
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestBooks.check_response(response, 200, 'books - skip: %d; limit: %d' % (skip, limit))
        TestGetBooks.check_pagination(response, 3, skip, limit, 1)

    def test_should_skip_all_books(self, client):
        # given
        skip = 2
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestBooks.check_response(response, 200, 'books - skip: %d; limit: %d' % (skip, limit))
        TestGetBooks.check_pagination(response, 3, skip, limit, 0)

    def test_should_return_error_for_negative_skip(self, client):
        # given
        skip = -1
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestGetBooks.check_error_message(response)

    def test_should_return_error_for_negative_limit(self, client):
        # given
        skip = 0
        limit = -2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestGetBooks.check_error_message(response)

    def test_should_return_error_for_invalid_skip(self, client):
        # given
        skip = 'invalid'
        limit = 2
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestGetBooks.check_error_message(response)

    def test_should_return_error_for_invalid_limit(self, client):
        # given
        skip = 0
        limit = 'invalid'
        arguments = {
            'skip': skip,
            'limit': limit
        }

        # when
        response = client.get('/v1/books/', query_string=arguments)

        # then
        TestGetBooks.check_error_message(response)

    @staticmethod
    def check_pagination(response, count, skip, limit, results_number):
        assert 'count' in response.json['content']
        assert 'skip' in response.json['content']
        assert 'limit' in response.json['content']
        assert 'results' in response.json['content']

        assert response.json['content']['count'] == count
        assert response.json['content']['skip'] == skip
        assert response.json['content']['limit'] == limit
        assert len(response.json['content']['results']) == results_number

    @staticmethod
    def check_error_message(response):
        assert 'errors' in response.json
        assert 'message' in response.json
        assert response.json['message'] == 'Input payload validation failed'
