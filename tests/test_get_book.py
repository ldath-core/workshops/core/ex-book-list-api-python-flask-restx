from pytest import mark

from test_fixtures import *
from tests.test_books import TestBooks


@mark.usefixtures('client')
class TestGetBook:

    def test_should_get_book_with_description(self, client):
        # given
        id = 1

        # when
        response = client.get('/v1/books/' + str(id))

        # then
        TestBooks.check_response(response, 200, 'book')
        TestBooks.check_response_content(response, id, 'Golang is great', 'Mr. Great', '2012')

        assert 'description' in response.json['content']
        assert response.json['content']['description'] == 'test'

    def test_should_get_book_with_img_url(self, client):
        # given
        id = 2

        # when
        response = client.get('/v1/books/' + str(id))

        # then
        TestBooks.check_response(response, 200, 'book')
        TestBooks.check_response_content(response, id, 'C++ is greatest', 'Mr. C++', '2015')

        assert 'image' in response.json['content']
        assert 'type' in response.json['content']['image']
        assert 'data' in response.json['content']['image']
        assert response.json['content']['image']['type'] == 'url'
        assert response.json['content']['image']['data'] == 'http://test.com'

    def test_should_get_book_with_img_base64(self, client):
        # given
        id = 3

        # when
        response = client.get('/v1/books/' + str(id))

        # then
        TestBooks.check_response(response, 200, 'book')
        TestBooks.check_response_content(response, id, 'C++ is very old', 'Mr. Old', '2014')

        assert 'image' in response.json['content']
        assert 'type' in response.json['content']['image']
        assert 'data' in response.json['content']['image']
        assert response.json['content']['image']['type'] == 'base64'
        assert response.json['content']['image']['data'] == 'aW1n'

    def test_should_return_not_found(self, client):
        # given
        id = 10

        # when
        response = client.get('/v1/books/' + str(id))

        # then
        TestBooks.check_error_response(response, 404, 'there is no documents with this id')
