class TestBooks:

    @staticmethod
    def check_response(response, status, message):
        assert 'status' in response.json
        assert 'message' in response.json
        assert 'content' in response.json

        assert response.status_code == status
        assert response.json['status'] == status
        assert response.json['message'] == message

    @staticmethod
    def check_response_content(response, id, title, author, year):
        assert 'id' in response.json['content']
        assert 'title' in response.json['content']
        assert 'author' in response.json['content']
        assert 'year' in response.json['content']

        assert response.json['content']['id'] == id
        assert response.json['content']['title'] == title
        assert response.json['content']['author'] == author
        assert response.json['content']['year'] == year

    @staticmethod
    def check_error_response(response, status, message):
        assert 'status' in response.json
        assert 'message' in response.json
        assert 'errors' in response.json

        assert response.json['status'] == status
        assert response.json['message'] == message
