from pytest import mark

from test_fixtures import *
from tests.test_books import TestBooks


@mark.usefixtures('client')
class TestDeleteBook:

    def test_should_delete_book(self, client):
        # given
        id = 1

        # when
        response = client.delete('/v1/books/' + str(id))

        # then
        TestBooks.check_response(response, 202, 'book: %d deleted' % id)

    def test_should_return_not_found(self, client):
        # given
        id = 10

        # when
        response = client.delete('/v1/books/' + str(id))

        # then
        TestBooks.check_error_response(response, 404, 'there is no documents with this id')
