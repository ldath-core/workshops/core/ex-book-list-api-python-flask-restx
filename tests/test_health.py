from os import environ

from pytest import mark

from test_fixtures import *


@mark.usefixtures('client')
class TestHealth:

    def test_get_health(self, client):
        # when
        response = client.get('/v1/health/')

        # then
        TestHealth.check_response(response, 200, 'ex-book-list-api-go api health')

        assert 'alive' in response.json['content']
        assert 'postgres' in response.json['content']

        assert response.json['content']['alive']
        assert response.json['content']['postgres']


    @staticmethod
    def check_response(response, status, message):
        assert 'status' in response.json
        assert 'message' in response.json
        assert 'content' in response.json

        assert response.json['status'] == status
        assert response.json['message'] == message
