from pytest import mark

from test_fixtures import *
from tests.test_books import TestBooks


@mark.usefixtures('client')
class TestCreateBooks:

    def test_should_create_book(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 201, 'book: 4 created')
        assert 'id' in response.json['content']
        assert response.json['content']['id'] == 4

    def test_should_create_book_with_description(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'description': 'test'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 201, 'book: 4 created')
        assert 'id' in response.json['content']
        assert response.json['content']['id'] == 4

    def test_should_create_book_with_img_url(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'url',
                'data': 'http://test.com'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 201, 'book: 4 created')
        assert 'id' in response.json['content']
        assert response.json['content']['id'] == 4

    def test_should_create_book_with_img_base64(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'base64',
                'data': 'aW1n'}}

        # when
        response = client.post('/v1/books/', json=book)

        # thenTestBooks.check_response(response, 201, 'book: 4 created')
        assert 'id' in response.json['content']
        assert response.json['content']['id'] == 4

    def test_should_reject_request_without_required_field(self, client):
        # given
        book = {
            'author': 'Test Author',
            'year': '2023'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'title key is required')

    def test_should_reject_request_with_invalid_value_type(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': [],
            'year': '2023'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'author must be string')

    def test_should_reject_request_with_empty_field_value(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': ''}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'year must not be empty')

    def test_should_reject_request_with_blank_field_value(self, client):
        # given
        book = {
            'title': ' ',
            'author': 'Test Author',
            'year': '2023'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'title must not be blank')

    def test_should_reject_request_with_unexpected_field(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'unexpected': 'unexpected'}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'only keys: title, author, year, description, image are allowed')

    def test_should_reject_request_with_invalid_description_value_type(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'description': 1}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'description must be string')

    def test_should_reject_request_with_empty_type_field_value(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': '',
                'data': 'aW1n'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'type must not be empty')

    def test_should_reject_request_with_blank_data_field_value(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'url',
                'data': ' '}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'data must not be blank')

    def test_should_reject_request_with_unexpected_img_field(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'url': 'http://test.com',
                'description': 'unexpected'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'only keys: type, data are allowed')

    def test_should_reject_request_with_type_but_without_data(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'base64'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'image data is required if image type is set')

    def test_should_reject_request_with_data_but_without_type(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'data': 'http://test.com'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'image type is required if image data is set')

    def test_should_reject_request_with_invalid_type(self, client):
        # given
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'invalid',
                'data': 'http://test.com'}}

        # when
        response = client.post('/v1/books/', json=book)

        # then
        TestBooks.check_response(response, 400, 'image type must be one of: base64, url')
