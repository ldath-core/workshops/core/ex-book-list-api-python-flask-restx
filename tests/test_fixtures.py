from pytest import fixture

from app.app import create_app
from app.model.book import db, Book


@fixture()
def app():
    app = create_app()

    test_data = (
        Book('Golang is great', 'Mr. Great', '2012', description='test'),
        Book('C++ is greatest', 'Mr. C++', '2015', img_type='url', img_data='http://test.com'),
        Book('C++ is very old', 'Mr. Old', '2014', img_type='base64', img_data='aW1n')
    )

    with app.app_context():
        db.drop_all()
        db.create_all()
        db.session.add_all(test_data)
        db.session.commit()

    yield app


@fixture()
def client(app):
    return app.test_client()


@fixture()
def runner(app):
    return app.test_cli_runner()
