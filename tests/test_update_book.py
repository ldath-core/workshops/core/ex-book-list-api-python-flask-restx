from pytest import mark

from test_fixtures import *
from tests.test_books import TestBooks


@mark.usefixtures('client')
class TestUpdateBook:

    def test_should_update_book_title(self, client):
        # given
        id = 1
        book = {'title': 'Test title'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, book['title'], 'Mr. Great', '2012')

    def test_should_update_book_author(self, client):
        # given
        id = 1
        book = {'author': 'Test Author'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, 'Golang is great', book['author'], '2012')

    def test_should_update_book_year(self, client):
        # given
        id = 1
        book = {'year': '2023'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, 'Golang is great', 'Mr. Great', book['year'])

    def test_should_update_book_description(self, client):
        # given
        id = 1
        book = {'description': 'test'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, 'Golang is great', 'Mr. Great', '2012')

        assert 'description' in response.json['content']
        assert response.json['content']['description'] == book['description']

    def test_should_update_book_img_url(self, client):
        # given
        id = 1
        book = {
            'image': {
                'type': 'url',
                'data': 'http://test.com'}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, 'Golang is great', 'Mr. Great', '2012')

        assert 'image' in response.json['content']
        assert 'type' in response.json['content']['image']
        assert 'data' in response.json['content']['image']
        assert response.json['content']['image']['type'] == book['image']['type']
        assert response.json['content']['image']['data'] == book['image']['data']

    def test_should_update_book_img_base64(self, client):
        # given
        id = 1
        book = {
            'image': {
                'type': 'base64',
                'data': 'aW1n'}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, 'Golang is great', 'Mr. Great', '2012')

        assert 'image' in response.json['content']
        assert 'type' in response.json['content']['image']
        assert 'data' in response.json['content']['image']
        assert response.json['content']['image']['type'] == book['image']['type']
        assert response.json['content']['image']['data'] == book['image']['data']

    def test_should_update_all_book_fields(self, client):
        # given
        id = 1
        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'description': 'test',
            'image': {
                'type': 'base64',
                'data': 'aW1n'}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_response(response, 202, 'book: %d updated' % id)
        TestBooks.check_response_content(response, id, book['title'], book['author'], book['year'])

        assert 'description' in response.json['content']
        assert response.json['content']['description'] == book['description']


    def test_should_reject_request_with_empty_body(self, client):
        # given
        id = 1
        book = {}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'request body must not be empty')

    def test_should_reject_request_with_invalid_value_type(self, client):
        # given
        id = 1
        book = {'author': []}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'author must be string')

    def test_should_reject_request_with_empty_field_value(self, client):
        # given
        id = 1
        book = {'year': ''}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'year must not be empty')

    def test_should_reject_request_with_blank_field_value(self, client):
        # given
        id = 1
        book = {'title': ' '}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'title must not be blank')

    def test_should_reject_request_with_unexpected_field(self, client):
        # given
        id = 1
        book = {'unexpected': 'unexpected'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'only keys: title, author, year, description, image are allowed')

    def test_should_return_not_found(self, client):
        # given
        id = 10
        book = {'title': 'Test title'}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 404, 'there is no documents with this id')

    def test_should_reject_request_with_invalid_description_value_type(self, client):
        # given
        id = 1

        book = {'description': 1}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'description must be string')

    def test_should_reject_request_with_empty_type_field_value(self, client):
        # given
        id = 1
        book = {'image': {'type': ''}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'type must not be empty')

    def test_should_reject_request_with_blank_data_field_value(self, client):
        # given
        id = 1
        book = {'image': {'data': ' '}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'data must not be blank')

    def test_should_reject_request_with_unexpected_img_field(self, client):
        # given
        id = 1

        book = {'image': {'description': 'unexpected'}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'only keys: type, data are allowed')

    def test_should_reject_request_with_invalid_type(self, client):
        # given
        id = 1

        book = {
            'title': 'Test title',
            'author': 'Test Author',
            'year': '2023',
            'image': {
                'type': 'invalid',
                'data': 'aW1n'}}

        # when
        response = client.put('/v1/books/' + str(id), json=book)

        # then
        TestBooks.check_error_response(response, 400, 'image type must be one of: base64, url')
