FROM python:3.11.2-slim-buster

COPY app ./app
COPY secrets ./secrets

WORKDIR /app
RUN pip3 install --upgrade pip && \
 apt-get update && apt-get install -y libpq-dev gcc && \
 pip3 install -r requirements.txt  && \

EXPOSE 8080

ENV CFG_FILE=/secrets/local.env.yaml

CMD [ "flask", "run", "--host", "0.0.0.0", "--port", "8080" ]
